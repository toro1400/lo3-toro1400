package se.miun.dsv.toro1400.social.web.servlet;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.toro1400.social.dao.SocialDAO;
import se.miun.dsv.toro1400.social.model.Event;
import se.miun.dsv.toro1400.social.model.User;

/**
 * Servlet implementation class EventCreateController
 */
@WebServlet("/event/create")
public class EventCreateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AtomicInteger hits;

	@Override
	public void init() {
		hits = new AtomicInteger(0);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("hits", hits.incrementAndGet());
		SocialDAO dao = (SocialDAO) getServletContext().getAttribute("socialDao");
		List<User> users = dao.findAllUsers();
		request.setAttribute("users", users);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/eventCreate.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yy-MM-dd HH:mm");
		String title = request.getParameter("title");
		String city = request.getParameter("city");
		String content = request.getParameter("content");
		String startTimeString = request.getParameter("start_time");
		String stopTimeString = request.getParameter("stop_time");
		LocalDateTime startTime = null;
		LocalDateTime stopTime = null;
		try {
			startTime = LocalDateTime.parse(startTimeString, dateFormat);
		} catch (Exception e) {
			startTime = LocalDateTime.now();
		}
		try {
			stopTime = LocalDateTime.parse(stopTimeString, dateFormat);
		} catch (Exception e) {
			stopTime = LocalDateTime.now();
		}
		
		Event e = new Event();
		e.setTitle(title);
		e.setCity(city);
		e.setContent(content);
		e.setStartTime(startTime);
		e.setEndTime(stopTime);
		Set<User> organizers = new HashSet<User>();
		
		SocialDAO dao = (SocialDAO) getServletContext().getAttribute("socialDao");
		List<User> users = dao.findAllUsers();
		User theUser = null;
		long userId = Long.parseLong(request.getParameter("dropbox"));
		for (User u : users) {
			if (u.getId() == userId) {
				theUser = u;
				break;
			}
		}
		organizers.add(theUser);
		e.setOrganizers(organizers);
		
		dao.addEvent(e);
		RequestDispatcher rd = request.getRequestDispatcher("/event/overview");
		rd.forward(request, response);
	}

}
