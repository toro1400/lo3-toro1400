package se.miun.dsv.toro1400.social.web.servlet;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.toro1400.social.dao.SocialDAO;
import se.miun.dsv.toro1400.social.model.Event;

/**
 * Servlet implementation class EventView
 */
@WebServlet("/event/view")
public class EventView extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AtomicInteger hits;

	@Override
	public void init() {
		hits = new AtomicInteger(0);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("hits", hits.incrementAndGet());
		
		long eventId = Long.parseLong(request.getParameter("id"));
		SocialDAO dao = (SocialDAO) getServletContext().getAttribute("socialDao");
		List<Event> events = dao.findAllEvents();
		Event theEvent = null;
		for (Event e : events) {
			if (e.getId() == eventId) {
				theEvent = e;
				break;
			}
		}
		request.setAttribute("event", theEvent);
		RequestDispatcher rd = request.getRequestDispatcher("//WEB-INF/view/eventView.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
