package se.miun.dsv.toro1400.social.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.dsv.toro1400.social.model.Comment;
import se.miun.dsv.toro1400.social.model.Event;
import se.miun.dsv.toro1400.social.model.User;

public class SocialDAO {
	private EntityManagerFactory emf;
	
	public SocialDAO() {
		this.emf = Persistence.createEntityManagerFactory("social");
	}
	
	public void shutdown() {
		if (emf != null)
			emf.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> findAllEvents() {
		EntityManager em = emf.createEntityManager();
		return (List<Event>) em.createQuery("SELECT e FROM Event e ORDER BY e.startTime").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		EntityManager em = emf.createEntityManager();
		return (List<User>) em.createQuery("SELECT u FROM User u").getResultList();
	}
	
	public void addEvent(Event e) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.persist(e);
			tx.commit(); 
		} catch (Exception event_e) {
			event_e.printStackTrace();
			tx.rollback();
		}	
	}
	
	public void addUser(User u) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.persist(u);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
	}
	
	public void persistAll(Set<User> users, Set<Event> events, Set<Comment> comments) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			for (User u : users)
				em.persist(u);
			for (Event e : events)
				em.persist(e);
			for (Comment c : comments)
				em.persist(c);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
	}
}
