package se.miun.dsv.toro1400.social.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import se.miun.dsv.toro1400.social.model.Comment;
import se.miun.dsv.toro1400.social.model.Event;
import se.miun.dsv.toro1400.social.model.User;

/**
 * A parser to read a file containing users, events and comments
 * All inter-object relationships are set up by the parser
 * @author frni1203
 */
public class EventStreamParser {
	private Set<User> users;
	private Set<Event> events;
	private Set<Comment> comments;
	private BufferedReader br;
	private boolean parsed;
	
	/**
	 * ctor
	 * @param is Open InputStream at the beginning of events file
	 */
	public EventStreamParser(InputStream is) {
		this(is, StandardCharsets.UTF_8.newDecoder());
	}
	
	public EventStreamParser(InputStream is, CharsetDecoder decoder) {
		this.br = new BufferedReader(new InputStreamReader(is, decoder));
		users = new HashSet<User>();
		events = new HashSet<Event>();
		comments = new HashSet<Comment>();
		parsed = false;
	}
	
	private void ensureParsed() throws ParserException {
		if(!parsed)
			throw new ParserException("Must call parseEvents() before attempting to get results");
	}
	
	public Set<Comment> getComments() throws ParserException {
		ensureParsed();
		return comments;
	}
	
	public Set<Event> getEvents() throws ParserException {
		ensureParsed();
		return events;
	}
	
	public Set<User> getUsers() throws ParserException {
		ensureParsed();
		return users;
	}
	
	/**
	 * Parses users, events and comments. After call, results may be fetched with respective getters. 
	 * @throws IOException
	 * @throws ParserException
	 */
	public void parseEvents() throws IOException, ParserException {
		UserParser userParser = new UserParser(this.br);
		EventParser eventParser = new EventParser(this.br, users);
		CommentParser commentParser = null;
		
		User user = null;
		while((user = userParser.next()) != UserParser.nullUser) {
			users.add(user);
		}
		
		Event event = null;
		while((event = eventParser.next()) != EventParser.nullEvent) {
			events.add(event);
			Comment comment = null;
			commentParser = new CommentParser(this.br, users, event);
			while((comment = commentParser.next()) != CommentParser.nullComment) {
				comments.add(comment);
			}
		}
		parsed = true;
	}
}