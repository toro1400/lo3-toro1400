package se.miun.dsv.toro1400.social.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import se.miun.dsv.toro1400.social.dao.SocialDAO;
import se.miun.dsv.toro1400.social.parsers.EventStreamParser;

/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {

	
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		SocialDAO dao = (SocialDAO)arg0.getServletContext().getAttribute("socialDao");
		dao.shutdown();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		SocialDAO dao = new SocialDAO();
		InputStream is = null;
		try {
			URL inputFileURL = new URL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt");
			is = inputFileURL.openStream();
			EventStreamParser esp = new EventStreamParser(is);
			esp.parseEvents();
			dao.persistAll(esp.getUsers(), esp.getEvents(), esp.getComments());
		} catch (Exception e) {
			System.err.println("Couldn't populate database: " + e.getMessage());
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (IOException e) {}
		}
		arg0.getServletContext().setAttribute("socialDao", dao);	
	}
}
