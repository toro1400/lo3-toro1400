<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
	<body>
		<div id="header">
			<h1>${headerText}</h1>
		</div>
		<div id="nav">
			<ul>
				<c:url value="/event/overview" var="eventOverviewLink" />
				<li><a href="${eventOverviewLink}">Event Overview</a></li>
				<c:url value="/event/create" var="createEventLink" scope="request"/>
				<li><a href="${createEventLink}">Create Event</a></li>
				<c:url value="/user/profile" var="userProfileLink" />
				<li><a href="${userProfileLink}">User Profile</a></li>
			</ul>
		</div>