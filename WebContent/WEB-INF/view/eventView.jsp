<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Event View</title>
</head>
<body>

<p>Title: ${event.title}</p>
<p>City: ${event.city}</p>
<p>Content: ${event.content}</p>
<p>Organizers:<br>
<ul>
<c:set value="${event.organizers}" var="organizers" />
<c:forEach items="${organizers}" var="organizer" >
		<c:url value="/user/profile" var="userLink">
			<c:param name="id" value="${organizer.id}" />
		</c:url>
		<li><a href="${userLink}"><c:out value="${organizer.lastName}" />, <c:out value="${organizer.firstName}" /></a></li>
	</c:forEach>
</ul>
<p>Comments:<br>
<ul>
	<c:forEach items="${event.comments}" var="comment" >
		<li><c:out value="${comment.comment}" /></li>
	</c:forEach>
</ul>



</body>
</html>