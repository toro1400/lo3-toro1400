<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   
<%@ page import="se.miun.dsv.toro1400.social.model.Event" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Event Overiew</title>
	</head>
	<c:set var="headerText" value="Event Overview" scope="request"/>
	<jsp:include page="pageHeader.jsp" />
	
		<div id="main">
		
			<ul>
				<c:forEach items="${events}" var="event">
					<li>
						<c:url value="/event/view" var="eventLink">
							<c:param name="id" value="${event.id}" />
						</c:url>
						<a href="${eventLink}">
							<span class="event_title"><c:out value="${event.title}" /> </span>
							<span class="event_city"><c:out value="${event.city}" /> </span>
							<span class="event_date"><c:out value="${event.startTime}" /> </span>
						</a>
					</li>
				</c:forEach>
			</ul>
			<div id="adder">
				<a href="${createEventLink}">New Event</a>
			</div>
		</div>
		<div id="footer">
			<p>Page counter: <c:out value="${hits}" /></p>
		</div>	
	</body>
</html>